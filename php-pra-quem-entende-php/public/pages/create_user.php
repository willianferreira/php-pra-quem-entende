<?= get('message') ?>

<form action="/pages/forms/create_user.php" method="POST" role="form">
    
    <div class="form-group">
        <label for="name">Nome</label>
        <input type="text" name="name" id="name" class="form-control" placeholder="Digiteu seu nome">
    </div>

    <div class="form-group">
        <label for="last_name">Sobrenome</label>
        <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Digiteu seu sobrenome">
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" name="email" id="email" class="form-control" placeholder="Digiteu seu email">
    </div>
    
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" id="password" class="form-control" placeholder="Digiteu sua senha">
    </div>

    <button type="submit" class="btn btn-primary">Cadastrar</button>
</form>