<?php

use app\models\Post;
use app\models\User;
use app\classes\Validation;
use app\models\Transaction;

$validation = new Validation;
$validate = $validation->validate($_POST);

$user = new User;
$cadastrado = $user->insert($validate);

if($cadastrado) {

    header('location:/');

}

// Exemplo Transactions
/*
$transaction = new Transaction;

$transaction->transactions(function () use ($transaction, $validate) {

    // $transaction->user->insert($validate);
    // $transaction->post->insert([
    //     'title' => 'teste',
    //     'user' => 1,
    //     'description' => 'teste'
    // ]);

    $transaction->model(User::class)->insert($validate);
    $transaction->model(Post::class)->insert([
        'title' => 'teste',
        'user' => 1,
        'description' => 'teste'
    ]);
});
*/

