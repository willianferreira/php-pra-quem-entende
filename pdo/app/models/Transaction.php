<?php

namespace app\models;

use Closure;

class Transaction extends Model
{
    /**
     * Inicia uma transaction. Tenta executar as querys, caso não de
     * os dados voltam para o estado anterior
     * 
     * @param Closure $callback
     */
    public function transactions(Closure $callback)
    {
        $this->connection->beginTransaction();
        
        try {
            $callback();
            $this->connection->commit();
        } catch(\Excpetion $e) {
            $this->connection->rollback();
            echo $e->getMessage();
        }
    }

    /**
     * Retorna a instância da model passada como parâmetro; 
     * 
     * @param Class $model
     * @return Model $model
     */
    public function model($model)
    {
        return new $model;
    }

    /**
     * Verifica se existe a model com aquele namespace e retorna a model;
     * 
     * @param String $name
     * @return Class Retorna uma classe (Model)
     */
    public function __get($name)
    {
        if(!property_exists($this, $name)) {
            $model = __NAMESPACE__ . '\\' . ucfirst($name);

            return new $model;
        }
    }

    

}