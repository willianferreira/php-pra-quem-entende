

<div class="card">

    <header class="card-header">
        <p class="card-header-title title is-4">
           Editar Usuário
        </p>
    </header>

    <div class="card-content">

        <form action="/user_update" method="POST">

            <input type="hidden" name="id" value="<?= $user->id ?>">

            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="nome">Nome</label>
                        <div class="control">
                            <input class="input" type="text" id="nome" placeholder="Digite seu nome" name="name" value="<?= $user->name ?>">
                        </div>
                    </div>
                </div>
            </div>    
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="email">E-mail</label>
                        <div class="control">
                            <input class="input" type="email" id="email" placeholder="exemplo@email.com" name="email" value="<?= $user->email ?>">
                        </div>
                    </div>
                </div>

                <div class="column">
                    <div class="field">
                        <label class="label" for="password">Password</label>
                        <div class="control">
                            <input class="input" type="password" id="password" placeholder="Digite sua senha" name="password" value="<?= $user->password ?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="columns">
                <div class="column">
                    <div class="field is-grouped is-grouped-right">
                        <p class="control">
                            <a href="/" type="button" class="button is-danger">
                                Cancelar
                            </a>
                        </p>
                        <p class="control">
                            <button type="submit" class="button is-primary">
                                Salvar
                            </button>
                        </p>
                    </div>
                </div>
            <div>
        
        </form>

    </div>

</div>