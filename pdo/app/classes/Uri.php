<?php

namespace app\classes;

class Uri {

    /**
     * Retorna a uri atual
     * @return String $uri atual
     */
    public static function load() {
        return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    }

}